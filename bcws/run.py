import websocket
import threading
import redis
import json
import time


class Saver:
    def __init__(self, host='localhost', port=6379):
        self.r = redis.StrictRedis(host=host, port=port)

    def get_saver(self, market, curr1, curr2):
        def save_message(message):
            print market + curr1 + curr2 + " : " + message
            market_pair = market + ":" + curr1 + ":" + curr2
            asks = 'asks:' + market_pair
            bids = 'bids:' + market_pair
            m = json.loads(message)
            if "ok" in m and m['ok'] == 1:
                if m['type'] == 'sdepth':
                    self.r.delete(asks, bids)
                    for ask in m['sdepth']['return']['asks']:
                        self.r.hset(asks, ask[0], ask[1])
                    for bid in m['sdepth']['return']['bids']:
                        self.r.hset(bids, bid[0], bid[1])
                elif m['type'] == 'trades':
                    print "todo"

        return save_message


class Worker:
    msgSaver = lambda self, msg: 1

    def __init__(self, market, curr1, curr2):
        if market.isalnum() and curr1.isalnum() and curr2.isalnum():
            self.market = market
            self.curr1 = curr1
            self.curr2 = curr2
            url = Worker.urlPart + market + curr1 + curr2
            self.ws = websocket.WebSocketApp(url,
                                        on_message=lambda ws, m: self.msgSaver(m),
                                        on_error=Worker.on_error,
                                        on_close=Worker.on_close)
            self.ws.on_open = lambda ws: threading.Thread(target=Worker.ping, args=[ws]).start()

    @staticmethod
    def on_error(ws, error):
        print error

    @staticmethod
    def ping(ws):
        while ws.keep_running:
            threading._sleep(15)
            # print "ping sent"
            ws.send('ping')

    @staticmethod
    def on_close(ws):
        print "### closed ###"

    @staticmethod
    def on_open(ws):
        threading.Thread(target=Worker.ping, args=[ws]).start()

    urlPart = "wss://d2.bitcoinwisdom.com/?symbol="

    def run(self):
        r = lambda: self.ws.run_forever(sslopt={"cert_reqs": websocket.ssl.CERT_NONE})
        threading.Thread(target=r).start()

    def set_saver(self, saver):
        self.msgSaver = saver.get_saver(self.market, self.curr1, self.curr2)


def init_saver():
    r = redis.StrictRedis(host='localhost', port=6379)

    def save_message(message):
        print message
        m = json.loads(message)
        if "ok" in m and m['ok'] == 1:
            if m['type'] == 'sdepth':
                r.delete('asks', 'bids')
                for ask in m['sdepth']['return']['asks']:
                    r.hset('asks', ask[0], ask[1])
                for bid in m['sdepth']['return']['bids']:
                    r.hset('bids', bid[0], bid[1])
            elif m['type'] == 'trades':
                print "todo"
    return save_message

if __name__ == "__main__":
    # websocket.enableTrace(True)
    s = Saver()
    w1 = Worker("bitstamp", "btc", "usd")
    w1.set_saver(s)
    print "w1 has been initialized"
    w1.run()
    print "w1 has been started"
    w2 = Worker("btce", "btc", "eur")
    w2.set_saver(s)
    print "w2 has initialized"
    w2.run()
    print "w2 has been started"
